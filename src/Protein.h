#ifndef Protein__H__
#define Protein__H__

#include "Tools.h"
#include "Config.h"

#include <cmath>
#include <fstream>
#include <numeric>
#include <iostream>
#include <algorithm>

template <int PotentialSize, int CalculationSteps>
class Protein {
public:
    Protein (const Config& config)
        : _np_radius (config.np_radius)
        , _cutoff (config.cutoff)
    {
        parse_coordinate_file (config.coordinate_file);
        parse_structure_file (config.structure_file);
        center_protein();

        if (!config.rotation_file.empty()) {
            _rotation_handle.open (config.rotation_file.c_str());

            if (!_rotation_handle.is_open()) {
                std::cout << "Error: Failed to open rotation file '" << config.rotation_file << "' for writing\n";
                std::exit (EXIT_FAILURE);
            }

            for (int bead = 0; bead < size(); ++bead)
                _rotation_handle << "atom " << bead << " name MOL radius 2.5\n";
            _rotation_handle << '\n';
        }
    }

    void parse_coordinate_file (const std::string& filename) {
        std::ifstream handle (filename.c_str());

        if (!handle.is_open()) {
            std::cout << "Error: Could not find protein coordinate file '" + filename + "'\n";
            std::exit (EXIT_FAILURE);
        }

        std::string line;

        while (std::getline (handle, line)) {
            if (std::string_view(line).substr(0, 4) == "ATOM") {
                try {
                    _x.emplace_back(0.1 * std::stod(line.substr(30, 8)));
                    _y.emplace_back(0.1 * std::stod(line.substr(38, 8)));
                    _z.emplace_back(0.1 * std::stod(line.substr(46, 8)));
                } 
                catch (std::invalid_argument& ia) {
                    std::cout << "Error: Invalid position data in coordinate file '" + filename + "'\n";
                    std::exit (EXIT_FAILURE);
                }
            }
        }
    
        handle.close();
    }

    void parse_structure_file (const std::string& filename) {
        std::ifstream handle (filename.c_str());

        if (!handle.is_open()) {
            std::cout << "Error: Could not find protein structure file '" + filename + "'\n";
            std::exit (EXIT_FAILURE);
        }

        std::string line;

        while (std::getline (handle, line)) {
        
            if (std::string_view(line).substr(0, 4) == "MASS") {
                try {
                    _mass.emplace_back(std::stod(line.substr(22, 14)));
                }
                catch (std::invalid_argument& ia) {
                    std::cout << "Error: Invalid structure data in structure file '" + filename + "'\n";
                    std::exit (EXIT_FAILURE);
                }
            }
        }

        if (_x.size() != _mass.size()) {
            std::cout << "Error: The data in the coordinate file and the structure file did not match\n";
            std::exit (EXIT_FAILURE);
        }

        handle.close();
    }

    void center_protein() {
        const double mass0 = std::accumulate (std::cbegin (_mass), std::cend (_mass), 0.);

        const double x0 = std::inner_product (std::cbegin (_x), std::cend (_x), std::cbegin (_mass), 0.) / mass0;
        const double y0 = std::inner_product (std::cbegin (_y), std::cend (_y), std::cbegin (_mass), 0.) / mass0;
        const double z0 = std::inner_product (std::cbegin (_z), std::cend (_z), std::cbegin (_mass), 0.) / mass0;
        
        std::transform (std::begin(_x), std::end(_x), std::begin(_x), [&](double& x) { return x - x0; });
        std::transform (std::begin(_y), std::end(_y), std::begin(_y), [&](double& y) { return y - y0; });
        std::transform (std::begin(_z), std::end(_z), std::begin(_z), [&](double& z) { return z - z0; });
    }

    Cache<CalculationSteps> calculate_cache (const std::array<double, CalculationSteps>& ssd, const int angle) {

        std::vector<double> x (size()), y(size()), z(size());

        const double phi   =  -5 * (angle % 72)  * (M_PI / 180); 
        const double theta =  (180 - 5 * (angle / 72)) * (M_PI / 180);

        double rotate[3][3]
        {
            { std::cos(theta) * std::cos(phi)       , -1.0 * std::cos(theta) * std::sin(phi), std::sin(theta) },
            { std::sin(phi)                         , std::cos(phi)                         , 0.0             },
            { -1.0 * std::sin(theta) * std::cos(phi), std::sin(theta) * std::sin(phi)       , std::cos(theta) }
        };

        for (int i = 0; i < size(); ++i) {
            x[i] = _x[i] * rotate[0][0] + _y[i] * rotate[0][1] + _z[i] * rotate[0][2];
            y[i] = _x[i] * rotate[1][0] + _y[i] * rotate[1][1] + _z[i] * rotate[1][2];
            z[i] = _x[i] * rotate[2][0] + _y[i] * rotate[2][1] + _z[i] * rotate[2][2];
        }
        
        const double z0 = *std::min_element (std::cbegin(z), std::cend(z));
        std::transform (std::begin(z), std::end(z), std::begin(z), [z0](double z) { return z - z0; });

        if (_rotation_handle.is_open()) {
            _rotation_handle << "timestep indexed\n";
            for (int bead = 0; bead < size(); ++bead)
                _rotation_handle << bead << ' ' << x[bead] << ' ' << y[bead] << ' ' << z[bead] << '\n'; 
            _rotation_handle << '\n';
        }

        const double dr = ssd.at(1) - ssd.at(0);

        Cache<CalculationSteps> cache (size());

        for (int bead = 0; bead < size(); ++bead) {
            for (int step = 0; step < CalculationSteps; ++step) {
                
                double distance = distance_to_surface (x.at(bead), y.at(bead), z.at(bead) + ssd.at(step));

                if (distance < _cutoff) {
                    
                    const int    index  = static_cast<std::size_t>((distance / _cutoff) * (PotentialSize - 1.0));
                    const double offset = static_cast<double>((distance / dr) - static_cast<std::size_t>(distance / dr));

                    std::get<0>(cache.at(bead)).at(step) = index;
                    std::get<1>(cache.at(bead)).at(step) = { offset, offset - 1.0 };
                    std::get<2>(cache.at(bead)).at(step) = true;
                }
                else {
                    std::get<0>(cache.at(bead)).at(step) = 0;
                    std::get<1>(cache.at(bead)).at(step) = { 0.0, 0.0 };                   
                    std::get<2>(cache.at(bead)).at(step) = false;
                }
            }
        }

        return cache;
    }

    double distance_to_surface (const double x, const double y, const double z) const noexcept {
        return std::sqrt (x * x + y * y + z * z) - _np_radius;
    }

    int size() const noexcept {
        return _x.size();
    }

    private:
        std::vector<double> _x, _y, _z, _mass;
        const double        _np_radius;
        const double        _cutoff;
        std::ofstream       _rotation_handle;
};

#endif
