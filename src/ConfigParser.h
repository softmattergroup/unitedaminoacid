#ifndef CONFIG_PARSER__H__
#define CONFIG_PARSER__H__

#include <map>
#include <string>
#include <fstream>
#include <iostream>

class ConfigParser {
    public:
        ConfigParser (const int argc, const char * argv[], const std::string& filename)
            : _filename (filename)
        {
            parse_line (argc, argv);
            parse_file ();
        }

        void parse_line (const int argc, const char * argv[]) {
            for (int i = 1; i < argc; ++i) {
                std::string arg (argv[i]);

                std::size_t pos;

                if (arg.size() <= 2 || arg.substr(0, 2) != "--" || (pos = arg.find ('=')) == std::string::npos) {
                    std::cout << "Error: Invalid format command line argument '" << arg << "'\n";
                    std::exit (EXIT_FAILURE);
                }

                std::string key   = arg.substr (0, pos);
                std::string value = arg.substr (pos + 1, arg.size() - pos - 1);

                if (key.empty() || value.empty()) {
                    std::cout << "Error: Invalid key=value pair found on command line\n";
                    std::cout << "Error: key = '" << key << "', value = '" << value << "'\n";
                    std::exit (EXIT_FAILURE);
                }

                _line_args[key] = value;
            }

            if (_line_args.count("--config"))
                _filename = _line_args["--config"];
        }

        void parse_file () {
            std::ifstream handle (_filename.c_str());

            if (!handle.is_open()) {
                std::cout << "Error: Could not open config file '" << _filename << "'\n";
                std::exit (EXIT_FAILURE);
            }

            std::string line;

            while (std::getline (handle, line)) {
                if (line.empty() || line[0] == '#')
                    continue;

                remove_whitespace_chars (line);

                std::size_t pos = line.find ('=');

                if (pos == std::string::npos)
                    continue;

                std::string key   = line.substr (0, pos);
                std::string value = line.substr (pos + 1, line.size() - pos - 1);

                if (key.empty() || value.empty()) {
                    std::cout << "Error: Invalid key=value pair found in config file\n";
                    std::cout << "Error: key = '" << key << "', value = '" << value << "'\n";
                    std::exit (EXIT_FAILURE);
                }

                _file_args[key] = value;
            }

            handle.close();

            if (_file_args.empty())
                std::cout << "Warning: There were no key=value configs detected in config file '" << _filename << "'\n";
        }

        bool get_arg (std::string& arg, const std::string& line_key, const std::string& file_key) {
            if (_line_args.count(line_key)) {
                arg = _line_args[line_key];
                return true;
            }
            else if (_file_args.count(file_key)) {
                arg = _file_args[file_key];
                return true;
            }
            else {
                return false;
            }
        }

        std::string get_string (const std::string& line_key, const std::string& file_key, const std::string default_value) {
            std::string arg;
            if (get_arg (arg, line_key, file_key))
                return arg;
            return default_value;
        }

        double get_int (const std::string& line_key, const std::string& file_key, const int default_value) {
            std::string arg;
            if (get_arg (arg, line_key, file_key)) {
                return std::stoi(arg);
            }
            return default_value;
        }

        double get_double (const std::string& line_key, const std::string& file_key, const double default_value) {
            std::string arg;
            if (get_arg (arg, line_key, file_key)) {
                return std::stod(arg);
            }
            return default_value;
        }

        std::string get_required_string (const std::string& line_key, const std::string& file_key) {
            std::string arg;
            if (get_arg (arg, line_key, file_key)) {
                return arg;
            }
            std::cout << "Error: There were no configs available for '" << line_key << "' or '" << file_key << "'\n";
            std::cout << "Error: This is a requied parameter\n";
            std::exit (EXIT_FAILURE);
        }

        int get_required_int (const std::string& line_key, const std::string& file_key) {
            std::string arg;
            if (get_arg (arg, line_key, file_key)) {
                return std::stoi (arg);
            }
            std::cout << "Error: There were no configs available for '" << line_key << "' or '" << file_key << "'\n";
            std::cout << "Error: This is a requied parameter\n";
            std::exit (EXIT_FAILURE);
        }

        double get_required_double (const std::string& line_key, const std::string& file_key) {
            std::string arg;
            if (get_arg (arg, line_key, file_key)) {
                return std::stod (arg);
            }
            std::cout << "Error: There were no configs available for '" << line_key << "' or '" << file_key << "'\n";
            std::cout << "Error: This is a requied parameter\n";
            std::exit (EXIT_FAILURE);
        }

    private:
        void remove_whitespace_chars (std::string& str) {
            for (auto itr = std::begin(str); itr != std::end(str);)
                itr = std::isspace (*itr) ? str.erase(itr) : std::next(itr);
        }

    private:
        std::string                        _filename;
        std::map<std::string, std::string> _line_args;
        std::map<std::string, std::string> _file_args;
};

#endif