#include "Config.h"
#include "ConfigParser.h"
#include "GeneticAlgorithm.h"

int main (const int argc, const char * argv[]) {

    constexpr int CalculationSteps  = 128;
    constexpr int PotentialSize     = 256;
    constexpr int PopulationSize    = 32;
    
    ConfigParser parser (argc, argv, "uaa.conf");

    Config config;

    // Required Parameters
    config.coordinate_file       = parser.get_required_string ("--cg-pdb", "cg_pdb");
    config.structure_file        = parser.get_required_string ("--cg-top", "cg_top");
    config.reference_file        = parser.get_required_string ("--reference-map", "reference_map");
    config.np_radius             = parser.get_required_double ("--np-radius", "np_radius");

    // Potential Parameters
    config.cutoff                = parser.get_double ("--cutoff", "cutoff", 8.0);
    config.sigma_min             = parser.get_double ("--sigma-min", "sigma_min", 1.0);
    config.sigma_max             = parser.get_double ("--sigma-max", "sigma_max", 3.0);
    config.wall_min              = parser.get_double ("--wall-min", "wall_min", 500.0);
    config.wall_max              = parser.get_double ("--wall-max", "wall_max", 1000.0);
    config.well_min              = parser.get_double ("--well-min", "well_min", -20.0);
    config.well_max              = parser.get_double ("--well-max", "well_max", -2.0);
    config.hydration_min         = parser.get_double ("--hydration-min", "hydration_min", 1.0);
    config.hydration_max         = parser.get_double ("--hydration-max", "hydration_max", 10.0);
    config.hydration_size_min    = parser.get_double ("--hydration-size-min", "hydration_size_min", 1.0);
    config.hydration_size_max    = parser.get_double ("--hydration-size-max", "hydration_size_max", 10.0);
    config.hydration_sigma_min   = parser.get_double ("--hydration-sigma-min", "hydration_sigma_min", 2.0);
    config.hydration_sigma_max   = parser.get_double ("--hydration-sigma-max", "hydration_sigma_max", 4.0);

    // Calculation Parameters
    config.integration_start     = parser.get_double ("--integration-start", "integration_start", 0.2);
    config.integration_stop      = parser.get_double ("--integration-stop", "integration_stop", 2.2);

    // Genetic Algorithm Parameters
    config.generations           = parser.get_int ("--generations", "generations", 100);

    // Output Parameters
    config.potential_output_file = parser.get_string ("--potential-output-file", "potential_output_file", "");
    config.map_output_file       = parser.get_string ("--map-output-file", "map_output_file", "");

    config.rotation_file         = parser.get_string ("--rotation-file", "rotation_file", "");
    
    config.potential_tag         = parser.get_string ("--potential-tag", "potential_tag", "");
    config.potential_directory   = parser.get_string ("--potential-directory", "potential_directory", ".");
    config.map_tag               = parser.get_string ("--map-tag", "map_tag", "");
    config.map_directory         = parser.get_string ("--map-directory", "map_directory", ".");

    std::cout << "Info: Building ...\n";

    GeneticAlgorithm<PopulationSize, PotentialSize, CalculationSteps> genetic_algorithm (config);
    
    genetic_algorithm.run();

    return EXIT_SUCCESS;
}
