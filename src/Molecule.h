#ifndef MOLECULE__H__
#define MOLECULE__H__

#include "Tools.h"
#include "Config.h"

template <int PotentialSize>
class Molecule {
public:
    Molecule (const Config& config, int size)
        : _energies (size)
        , _size (size)
        , _cutoff (config.cutoff)
        , _cost (std::numeric_limits<double>::max())
    {
        for (int bead = 0; bead < size; ++bead) {
            switch (RandomInt(0, 2)) {
                case 0: // Repulsise Wall Only
                {
                    const double wall  = RandomDouble (config.wall_min, config.wall_max);
                    const double sigma = RandomDouble (config.sigma_min, config.sigma_max);
                    const double percentAtSigma = std::log (0.01); // The wall will be at this percent of original value at sigma

                    for (int i = 0; i < PotentialSize; ++i)
                        _energies[bead][i] = wall * std::exp (percentAtSigma * distance_at_index (i) / sigma);
                }
                break;
                case 1: // Repulsive Wall and Attractive Well
                {
                    const double wall   = RandomDouble (config.wall_min, config.wall_max);
                    const double well   = RandomDouble (config.well_min, config.well_max);
                    const double sigma  = RandomDouble (config.sigma_min, config.sigma_max);
                    const double lambda = 0.5 * (1.0 + std::sqrt(1.0 - wall / well));

                    double beta;
                    for (int i = 0; i < PotentialSize; ++i) {
                        beta = 1.0 - std::pow (distance_at_index (i) / sigma, 1.7); 
                        _energies[bead][i] = 4.0 * well * (std::pow(lambda, beta) - std::pow(lambda, 2.0 * beta));
                    }
                }
                break;
                case 2: // Repulsive Wall, Attracvive Well and Hydration Layer
                {
                    const double wall   = RandomDouble (config.wall_min, config.wall_max);
                    const double well   = RandomDouble (config.well_min, config.well_max);
                    const double sigma  = RandomDouble (config.sigma_min, config.sigma_max);
                    const double lambda = 0.5 * (1.0 + std::sqrt(1.0 - wall / well));
                    const double hydration       = RandomDouble(config.hydration_min, config.hydration_max);
                    const double hydration_size  = RandomDouble (config.hydration_size_min, config.hydration_size_max);
                    const double hydration_sigma = RandomDouble(config.hydration_sigma_min, config.hydration_sigma_max);
        
                    double beta;
                    for (int i = 0; i < PotentialSize; ++i) {
                        beta = 1.0 - std::pow(distance_at_index (i) / sigma, 1.7); 
                        _energies[bead][i] = 4.0 * well * (std::pow(lambda, beta) - std::pow(lambda, 2.0 * beta))
                            + hydration * std::exp(-1.0 * hydration_size * distance_at_index (i) / hydration_sigma);
                    }
                }
                break;
                default:
                    std::cout << "Error: Potential::initialize(): Recived an out of bound type\n";
                    std::exit (EXIT_FAILURE);
            }
    
            for (int i = 0; i < PotentialSize; ++i)
                _energies[bead][i] -= _energies[bead][PotentialSize - 1];
        }
    }

    Molecule (const Molecule&)              = delete;
    Molecule (Molecule&&)                   = delete;
    Molecule& operator = (const Molecule&)  = delete;
    Molecule& operator = (Molecule&&)       = delete;

    void cross (const Molecule<PotentialSize>* lhs, const Molecule<PotentialSize>* rhs) noexcept {
        for (int bead = 0; bead < size(); ++bead) {
            switch (RandomInt(0, 2)) {
                case 0: // Take the lhs potential
                    _energies[bead] = lhs->_energies[bead];
                    break;
                case 1: // Take the rhs potential
                    _energies[bead] = rhs->_energies[bead];
                    break;
                case 2: // Average the lhs and rhs potential
                    _energies[bead] = ArrayAverage<PotentialSize>(lhs->_energies[bead], rhs->_energies[bead]);
                    break;
                case 3: // Keep current potential
                break;
            }
        }
    }

    void mutate() noexcept {
        for (int bead = 0; bead < size(); ++bead) {
            
            int begin = RandomInt (0, PotentialSize);
            int end   = RandomInt (0, PotentialSize);
        
            if (begin > end)
                std::swap (begin, end);

            const double mutation = RandomDouble (-1, 1);
            const int    size     = end - begin;
            double p;

            for (int offset = 0; offset < size; ++offset) {
                p = offset / (size - 1.0);
                _energies[bead][begin + offset] += 16.0 * mutation * std::pow(p * (1.0 - p), 2.0);
            }
        }
    }

    //TODO: Can this be optimized?
    std::pair<double, double> energy_pair (int bead, int index) const noexcept {
        return { _energies[bead][index], _energies[bead][index + 1] };
    }

    void save_as (const std::string& filename) const noexcept {
        std::ofstream handle (filename.c_str());

        if (!handle.is_open()) {
            std::cout << "Error: Failed to open file for writing '" << filename << "'\n";
            std::exit (EXIT_FAILURE);
        }

        for (int row = 0; row < PotentialSize; ++row) {
            for (int col = 0; col < size(); ++col) {
                handle << distance_at_index (row) << ' ' << _energies[col][row] << (col == size() - 1 ? '\n' : ' ');
            }
        }

        handle.close();
    }

    double distance_at_index (const int index) const noexcept {
        return (index / (PotentialSize - 1.0)) * _cutoff;
    }

    double& cost() noexcept {
        return _cost;
    }

    double cost() const noexcept {
        return _cost;
    }

    int size() const noexcept {
        return _size;
    }

private:
    std::vector<std::array<double, PotentialSize>> _energies;
    const int       _size;
    const double    _cutoff;
    double          _cost;
};

template <int PotentialSize>
bool operator < (const Molecule<PotentialSize>& lhs, const Molecule<PotentialSize>& rhs) {
    return lhs.cost() < rhs.cost();
}

#endif