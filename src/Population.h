#ifndef POPULATION__H__
#define POPULATION__H__

#include "Tools.h"
#include "Config.h"
#include "Solver.h"
#include "Molecule.h"

#include <array>
#include <algorithm>

template <int PopulationSize, int PotentialSize, int CalculationSteps>
class Population {
public:
    Population (const Config& config)
        : _solver (config)
    {
        for (auto& molecule : _molecules)
            molecule = new Molecule<PotentialSize> (config, _solver.size());
    }

    ~Population() {
        for (auto& molecule : _molecules)
            delete molecule;
    }

    Population (const Population&)              = delete;
    Population (Population&&)                   = delete;
    Population& operator = (const Population&)  = delete;
    Population& operator = (Population &&)      = delete;

    void update () {
        for (auto& molecule : _molecules)
            molecule->cost() = _solver.solve (*molecule);
        sort();
    }

    void replace_weak_with_strong (Population<PopulationSize, PotentialSize, CalculationSteps>& other) noexcept {
        auto alpha = std::prev (std::end (_molecules));
        auto beta  = std::begin (other._molecules);

        while (**beta < **alpha) {
            std::swap(*alpha, *beta);
            --alpha;
            ++beta;
        }

        sort();
    }

    void cross (const Population<PopulationSize, PotentialSize, CalculationSteps>& other) noexcept {
        for (auto& molecule : _molecules)
            molecule->cross (other.random_member(), other.random_member());
    }

    void mutate () noexcept {
        for (auto& molecule : _molecules)
            molecule->mutate();
    }

    void sort() noexcept {
        std::sort (std::begin (_molecules), std::end (_molecules),
            [](const Molecule<PotentialSize>* lhs, const Molecule<PotentialSize>* rhs) {
                return lhs->cost() < rhs->cost();
            });
    }

    double mean_cost () const noexcept {
        return std::accumulate (std::cbegin(_molecules), std::cend(_molecules), 0.,
            [](const double sum, const Molecule<PotentialSize> * molecule) {
                return sum + molecule->cost();
            });
    }

    double lowest_cost () const noexcept {
        return _molecules.front()->cost();
    }

    void save_fittest_potential (const std::string& filename) const {
        _molecules.front()->save_as (filename);
    }

    void save_fittest_map (const std::string& filename) const {
        _solver.save_as (*(_molecules.front()), filename);
    }

    Molecule<PotentialSize> const * const random_member () const noexcept {
        return _molecules[RandomInt(0, PopulationSize - 1)];
    }

    private:
        Solver<PotentialSize, CalculationSteps>                  _solver;
        std::array<Molecule<PotentialSize>*, PopulationSize>     _molecules;
};

#endif