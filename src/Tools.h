#ifndef TOOLS__H__
#define TOOLS__H__

#include <array>
#include <tuple>
#include <random>
#include <vector>

///////////////////////////////////////////////////////////////////////////////////////////////////

std::default_random_engine engine;

std::size_t RandomInt(const std::size_t begin, const std::size_t end) {
    std::uniform_int_distribution<std::size_t> distribution (begin, end);
    return distribution (engine);
}

double RandomDouble (const double begin, const double end) {
    std::uniform_real_distribution<double> distribution (begin, end);
    return distribution (engine);
}

///////////////////////////////////////////////////////////////////////////////////////////////////

template <int ArraySize> using Cache = std::vector<std::tuple<std::array<int, ArraySize>, std::array<std::pair<double, double>, ArraySize>, std::array<bool, ArraySize>>>;

template <int ArraySize> std::array<double, ArraySize>& operator *= (std::array<double, ArraySize>& lhs, const std::array<double, ArraySize>& rhs) {
    for (int i = 0; i < ArraySize; ++i)
        lhs[i] *= rhs[i];
}

std::pair<double, double>& operator += (std::pair<double, double>& lhs, const std::pair<double, double>& rhs) {
    lhs.first += rhs.first;
    lhs.second += rhs.second;
    return lhs;
}

template <int ArraySize> std::array<double, ArraySize> ArrayAverage (const std::array<double, ArraySize>& lhs, const std::array<double, ArraySize>& rhs) {
    std::array<double, ArraySize> res;
    for (int i = 0; i < ArraySize; ++i)
        res[i] = (lhs[i] + rhs[i]) / 2.0;
    return res;
}

#endif