#ifndef ANGLE_SOLVER__H__
#define ANGLE_SOLVER__H__

#include <cmath>

#include "Tools.h"
#include "Molecule.h"

template <int PotentialSize, int CalculationSteps>
class AngleSolver {
    public:
        AngleSolver(const std::array<double, CalculationSteps>& ssd2, const int size, const Cache<CalculationSteps>& cache, const double factor, const double reference)
            : _ssd2 (ssd2)
            , _size (size)
            , _cache (cache)
            , _factor (factor)
            , _reference (reference)
        {}

        double solve(const Molecule<PotentialSize>& molecule) const noexcept {

            _energy_pairs = {};
            _energies     = {};
            _result       = 0.0;

            int i;

            for (int bead = 0; bead < _size; ++bead) {

                const auto& index    = std::get<0>(_cache[bead]);
                const auto& offset   = std::get<1>(_cache[bead]);
                const auto& is_valid = std::get<2>(_cache[bead]);

                for (i = 0; i < CalculationSteps; ++i) {
                    if (!is_valid[i])
                        continue;

                    _energy_pairs[i] +=  molecule.energy_pair (bead, index[i]);
                }

                for (i = 0; i < CalculationSteps; ++i)
                    _energies[i] += _energy_pairs[i].second * offset[i].first + _energy_pairs[i].first * offset[i].second;
            }

            _background_energy = _energies[CalculationSteps - 1];

            for (i = 0; i < CalculationSteps; ++i)
                _energies[i] = _energies[i] - _background_energy;

            for (i = 0; i < CalculationSteps; ++i)
                _energies[i] = std::exp(-1.0 * _energies[i]);

            for (i = 0; i < CalculationSteps; ++i)
                _result += _energies[i] * _ssd2[i];

            return std::pow (1.0 + std::log(_result * _factor) / _reference, 2.0);
        }

    private:
        mutable std::array<std::pair<double, double>, CalculationSteps> _energy_pairs;
        mutable std::array<long double, CalculationSteps>               _energies;
        mutable double                                                  _background_energy;
        mutable long double                                             _result;

        const Cache<CalculationSteps>                           _cache;
        const std::array<double, CalculationSteps>              _ssd2;
        const double                                            _factor;
        const double                                            _reference;
        const int                                               _size;
};

#endif