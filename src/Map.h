#ifndef MAP__H__
#define MAP__H__

#include "Config.h"

#include <array>
#include <fstream>
#include <iostream>

class Map {
    public:
        Map (const Config& config) {
            std::ifstream handle (config.reference_file.c_str());
        
            if (!handle.is_open()) {
                std::cout << "Error: Could not open map file '" << config.reference_file << "'\n";
                std::exit (EXIT_FAILURE);
            }

            std::size_t lineNumber = 0;
            std::string line;

            while (std::getline (handle, line)) {
                if (line.empty() || line[0] == '#')
                    continue;

                _energies[lineNumber++] = std::stod(line.substr(14, 14));
            }

            handle.close();
        }

        Map () = default;
        Map (const Map&) = delete;
        Map (Map&&) = delete;
        Map& operator = (const Map&) = delete;
        Map& operator = (Map&&) = delete;

        double energy (const int index) const noexcept {
            return _energies[index];
        }

    private:
        std::array<double, 2592> _energies;
};

#endif