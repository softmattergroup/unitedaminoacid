#ifndef SOLVER__H__
#define SOLVER__H__

#include "Map.h"
#include "Protein.h"
#include "AngleSolver.h"

#include <thread>
#include <omp.h>
#include <array>
#include <iomanip>

template <int PotentialSize, int CalculationSteps>
class Solver {
public:
    Solver (const Config& config) {        
        Protein<PotentialSize, CalculationSteps> protein (config);
        
        Map reference (config);

        _size = protein.size();

        std::array<double, CalculationSteps> ssd, ssd2;

        for (int step = 0; step < CalculationSteps; ++step) {
            ssd[step] = config.np_radius + config.integration_start
                + (config.integration_stop - config.integration_start) * (step / (CalculationSteps - 1.0));
            ssd2[step] = ssd[step] * ssd[step];
        }

        const double step_size = ssd[1] - ssd[0];

        const double factor
            = (3.0 * step_size) / (std::pow (config.integration_stop, 3.0) - std::pow (config.integration_start, 3.0));

        for (int angle = 0; angle < 2592; ++angle) {
            _angle_solvers[angle]
                = new AngleSolver<PotentialSize, CalculationSteps> (ssd, _size, protein.calculate_cache(ssd, angle), factor, reference.energy(angle));
        }
    }

    ~Solver () {
        for (auto& angle_solver : _angle_solvers)
            delete angle_solver;
    }

    Solver (const Solver&)              = delete;
    Solver (Solver&&)                   = delete;
    Solver& operator = (const Solver&)  = delete;
    Solver& operator = (Solver&&)       = delete;

    double solve (const Molecule<PotentialSize>& molecule) const noexcept {
        int i;
        double cost = 0;

        #pragma omp parallel for reduction(+:cost)
        for (i = 0; i < 2592; ++i) {
            cost += _angle_solvers[i]->solve (molecule);
        }
        return cost;
    }

    void save_as (const Molecule<PotentialSize>& molecule, const std::string& filename) const noexcept {
        int i;
        std::array<double, 2592> energies;

        #pragma omp parallel for
        for (i = 0; i < 2592; ++i) {
            energies[i] =  _angle_solvers[i]->solve (molecule);
        }

        std::ofstream handle (filename.c_str());

        if (!handle.is_open()) {
            std::cout << "Error: Failed to open '" << filename << "' for writing\n";
            std::exit (EXIT_FAILURE);
        }

        int phi, theta;

        for (int angle = 0; angle < 2592; ++angle) {
            
            phi   =  5 * (angle % 72);
            theta =  5 * (angle / 72);

            handle << std::left << std::setw(7) << phi;
            handle << std::left << std::setw(7) << theta;
            handle << std::left << std::setw(14) << std::fixed << std::setprecision(5) << energies[angle];
            handle << "\n";
        }

        handle.close();
    }

    int size() const noexcept {
        return _size;
    }

private:
    std::array<AngleSolver<PotentialSize, CalculationSteps>*, 2592> _angle_solvers;
    int _size;
};

#endif