#ifndef GENETIC_ALGORITHM__H__
#define GENETIC_ALGORITHM__H__

#include <chrono>

#include "Config.h"
#include "Population.h"

template <int PopulationSize, int PotentialSize, int CalculationSteps>
class GeneticAlgorithm {
public:
    GeneticAlgorithm (const Config& config)
        : _alpha_population (config)
        , _beta_population (config)
        , _generations (config.generations)
        , _saving_potentials (false)
        , _saving_maps (false)
    {
        if (!config.potential_tag.empty()) {
            _saving_potentials   = true;
            _potential_tag       = config.potential_tag;
            _potential_directory = config.potential_directory;
        }

        if (config.potential_output_file.empty()) {
            std::size_t pos         = config.reference_file.find_last_of ('.');
            _potential_output_file  = config.reference_file.substr(0, pos) + "_uaa_cg.pot";
        }
        else
            _potential_output_file = config.potential_output_file;

       if (!config.map_tag.empty()) {
            _saving_maps    = true;
            _map_tag        = config.map_tag;
            _map_directory  = config.map_directory;
        }

        if (config.map_output_file.empty()) {
            std::size_t pos     = config.reference_file.find_last_of ('.');
            _map_output_file    = config.reference_file.substr(0, pos) + "_uaa_cg.map";
        }
        else
            _map_output_file = config.map_output_file;


        _alpha_population.update();
        _beta_population.update();
        _alpha_population.replace_weak_with_strong (_beta_population);
    }

    GeneticAlgorithm (const GeneticAlgorithm&)              = delete;
    GeneticAlgorithm (GeneticAlgorithm&&)                   = delete;
    GeneticAlgorithm& operator = (const GeneticAlgorithm&)  = delete;
    GeneticAlgorithm& operator = (GeneticAlgorithm&&)       = delete;

    void run () {

        if (_saving_potentials)
            std::cout << "Info: Saving fittest potential at each step to " << _potential_directory << "\n";

        if (_saving_maps)
            std::cout << "Info: Saving fittest map at each step to " << _map_directory << "\n";

        std::cout << "Info: Running ...\n";

        for (int generation = 1; generation <= _generations; ++generation) {
        
            auto start = std::chrono::high_resolution_clock::now();

            _beta_population.cross (_alpha_population);
            _beta_population.mutate ();
            _beta_population.update ();
            _alpha_population.replace_weak_with_strong (_beta_population);

            const double mean_cost   = _alpha_population.mean_cost ();
            const double lowest_cost = _alpha_population.lowest_cost ();

            auto stop = std::chrono::high_resolution_clock::now();

            std::cout << generation << ' ' << mean_cost << ' ' << lowest_cost << '\n';
            std::cout << "Time taken: " << (std::chrono::duration_cast<std::chrono::milliseconds>(stop - start)).count() << "\n";

            if (_saving_potentials) {
                std::string filename = _potential_directory + '/' + _potential_tag
                    + '_' + std::to_string(generation) + ".dat";
                _alpha_population.save_fittest_potential (filename);
            }

            if (_saving_maps) {
                std::string filename = _map_directory + '/' + _map_tag
                    + '_' + std::to_string(generation) + ".dat";
                _alpha_population.save_fittest_map (filename);
            }
        }

        _alpha_population.save_fittest_potential(_potential_output_file);
        _alpha_population.save_fittest_map (_map_output_file);
    }

private:
    Population<PopulationSize, PotentialSize, CalculationSteps> _alpha_population;
    Population<PopulationSize, PotentialSize, CalculationSteps> _beta_population;
    std::size_t _generations;

    std::string _potential_output_file;
    std::string _map_output_file;

    bool        _saving_potentials;    
    std::string _potential_tag;
    std::string _potential_directory;

    bool        _saving_maps;
    std::string _map_tag;
    std::string _map_directory;
};

#endif