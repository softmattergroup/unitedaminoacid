#ifndef CONFIG__H__
#define CONFIG__H__

#include <string>

struct Config {
    // Required Parameters
    std::string     coordinate_file;
    std::string     structure_file;
    std::string     reference_file;
    double          np_radius;

    // Potential Parameters
    double cutoff;
    double sigma_min;
    double sigma_max;
    double wall_min;
    double wall_max;
    double well_min;
    double well_max;
    double hydration_min;
    double hydration_max;
    double hydration_size_min;
    double hydration_size_max;
    double hydration_sigma_min;
    double hydration_sigma_max;

    // Calculation Parameters
    double integration_start;
    double integration_stop;

    // Genetic Algorithm Parameters
    std::size_t generations;

    // Output Parameters
    std::string potential_output_file;
    std::string map_output_file;
    std::string rotation_file;
    std::string potential_tag;
    std::string potential_directory;
    std::string map_tag;
    std::string map_directory;
};

#endif
